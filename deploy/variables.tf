variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "jonn.black011696@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}
variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "416391012841.dkr.ecr.eu-west-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for Proxy"
  default     = "416391012841.dkr.ecr.eu-west-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret Key for Django"
}

variable "dns_zone_name" {
  description = "Route53 registered domain"
  default     = "northern-cleaning-service.co.uk"
}

variable "subdomain" {
  description = "Subdomain per env"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}